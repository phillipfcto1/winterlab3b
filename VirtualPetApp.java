import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Parrot[] flock = new Parrot[4];
		
		for(int i = 0; i < flock.length; i++){
				flock[i] = new Parrot();
				System.out.println("What is the name of your Parrot");
				flock[i].Name = reader.nextLine();
				System.out.println("What is the Height of your Parrot (in cm)");
				flock[i].Height = Integer.parseInt(reader.nextLine());
				System.out.println("Is your Parrot an adult (True or false)");
				flock[i].Adult = reader.nextBoolean();
				reader.nextLine();
		}
		System.out.println("Info about last bird");
		System.out.println("Name: " + flock[flock.length-1].Name);
		System.out.println("Is he an Adult: " + flock[flock.length-1].Adult);
		System.out.println("Height: " + flock[flock.length-1].Height);
		
		flock[0].Fly();
		flock[0].Height();
	}
}